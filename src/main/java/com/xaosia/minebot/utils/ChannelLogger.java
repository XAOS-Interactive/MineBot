package com.xaosia.minebot.utils;

import net.dv8tion.jda.core.MessageBuilder;
import com.xaosia.minebot.MineBot;
import com.xaosia.minebot.config.Settings;

public class ChannelLogger {

    public static void logChat(String message) {
        if (Settings.getChatLogId() != null) {
            Chat.sendMessage(message, MineBot.getClient().getTextChannelById(Settings.getChatLogId()));
        }
    }

    public static void logChat(MessageBuilder message) {
        if (Settings.getChatLogId() != null) {
            MineBot.getClient().getTextChannelById(Settings.getChatLogId()).sendMessage(message.build()).queue();
        }
    }

    public static void logCommand(String message) {
        if (Settings.getCommandLogId()!= null) {
            Chat.sendMessage(message, MineBot.getClient().getTextChannelById(Settings.getCommandLogId()));
        }
    }

    public static void logCommand(MessageBuilder message) {
        if (Settings.getCommandLogId() != null) {
            MineBot.getClient().getTextChannelById(Settings.getCommandLogId()).sendMessage(message.build()).queue();
        }
    }

    public static void logJoinLeave(String message) {
        if (Settings.getJoinLogId() != null) {
            Chat.sendMessage(message, MineBot.getClient().getTextChannelById(Settings.getJoinLogId()));
        }
    }

    public static void logJoinLeave(MessageBuilder message) {
        if (Settings.getJoinLogId() != null) {
            MineBot.getClient().getTextChannelById(Settings.getJoinLogId()).sendMessage(message.build()).queue();
        }
    }

    public static void logStatus(String message) {
        if (Settings.getStatusLogId() != null) {
            Chat.sendMessage(message, MineBot.getClient().getTextChannelById(Settings.getStatusLogId()));
        }
    }

    public static void logStatus(MessageBuilder message) {
        if (Settings.getStatusLogId() != null) {
            MineBot.getClient().getTextChannelById(Settings.getStatusLogId()).sendMessage(message.build()).queue();
        }
    }


}
