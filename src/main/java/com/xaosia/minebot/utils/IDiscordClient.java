package com.xaosia.minebot.utils;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import com.xaosia.minebot.MineBot;

import java.util.List;
import java.util.stream.Collectors;

public class IDiscordClient {

    public boolean isReady() {
        return MineBot.getClient().getStatus().equals(JDA.Status.CONNECTED);
    }

    public List<Guild> getRolesForGuild(Guild guild) {
        return MineBot.getClient().getGuilds().stream().filter(g -> g.getRoles().equals(guild)).collect(Collectors.toList());
    }

    public boolean userHasRoleId(Member member, String id) {
        return member.getRoles().contains(MineBot.getClient().getRoleById(id));
    }

    public void streaming(String status, String url) {
        MineBot.getClient().getPresence().setGame(Game.of(status, url));
    }

}
