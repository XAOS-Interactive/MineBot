package com.xaosia.minebot.utils;


import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import com.xaosia.minebot.MineBot;

public class WhitelistUtils {

    public static void whitelist(String user) {

        new BukkitRunnable(){
            @Override
            public void run() {
                CommandSender cs = Bukkit.getServer().getConsoleSender();
                MineBot.get().getServer().dispatchCommand(cs, "whitelist add " + user);
            }
        }.runTask(MineBot.get());

    }

    public static void unwhitelist(String user) {

        new BukkitRunnable(){
            @Override
            public void run() {
                CommandSender cs = Bukkit.getServer().getConsoleSender();
                MineBot.get().getServer().dispatchCommand(cs, "whitelist remove " + user);
            }
        }.runTask(MineBot.get());

    }

}
