package com.xaosia.minebot.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import com.xaosia.minebot.MineBot;

public class PunishUtils {

    public static void mute(String user, String time, String reason) {

        new BukkitRunnable(){
            @Override
            public void run() {
                CommandSender cs = Bukkit.getServer().getConsoleSender();
                MineBot.get().getServer().dispatchCommand(cs, "mute " + user + " " + time + " " + reason);
            }
        }.runTask(MineBot.get());

    }

    public static void ban(String user, String time, String reason) {

        new BukkitRunnable(){
            @Override
            public void run() {
                CommandSender cs = Bukkit.getServer().getConsoleSender();
                MineBot.get().getServer().dispatchCommand(cs, "ban " + user + " " + time + " " + reason);
            }
        }.runTask(MineBot.get());

    }

    public static void ban(String user, String reason) {

        new BukkitRunnable(){
            @Override
            public void run() {
                CommandSender cs = Bukkit.getServer().getConsoleSender();
                MineBot.get().getServer().dispatchCommand(cs, "ban " + user + " " + reason);
            }
        }.runTask(MineBot.get());

    }


}
