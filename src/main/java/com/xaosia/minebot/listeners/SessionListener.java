package com.xaosia.minebot.listeners;

import net.dv8tion.jda.core.MessageBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import com.xaosia.minebot.MineBot;
import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.ChannelLogger;
import com.xaosia.minebot.utils.Chat;

public class SessionListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        ChannelLogger.logJoinLeave(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Player Join", null)
                .setDescription(player.getName() + " has joined the server.")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_GREEN).build()));

        new BukkitRunnable(){
            @Override
            public void run() {
                Bot.updateUsers();
            }
        }.runTaskLater(MineBot.get(), 40);

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        ChannelLogger.logJoinLeave(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Player Quit", null)
                .setDescription(player.getName() + " has left the server.")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_RED).build()));

        new BukkitRunnable(){
            @Override
            public void run() {
                Bot.updateUsers();
            }
        }.runTaskLater(MineBot.get(), 40);
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        Player player = event.getPlayer();

        ChannelLogger.logJoinLeave(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Player Quit", null)
                .setDescription(player.getName() + " has left the server.")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_RED).build()));

        new BukkitRunnable(){
            @Override
            public void run() {
                Bot.updateUsers();
            }
        }.runTaskLater(MineBot.get(), 40);
    }

}
