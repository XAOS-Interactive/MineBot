package com.xaosia.minebot.listeners;

import com.xaosia.minebot.utils.ChannelLogger;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();

        ChannelLogger.logChat(player.getName() + ": " + ChatColor.translateAlternateColorCodes('&', message));
    }

}
