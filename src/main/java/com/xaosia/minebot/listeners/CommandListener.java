package com.xaosia.minebot.listeners;

import net.dv8tion.jda.core.MessageBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.ChannelLogger;
import com.xaosia.minebot.utils.Chat;

public class CommandListener implements Listener {


    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();

        ChannelLogger.logCommand(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Command Logger", null)
                .setDescription(event.getMessage() + "\n" + "player: " + player.getName())
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_ORANGE).build()));

    }

}
