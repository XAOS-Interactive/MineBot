package com.xaosia.minebot;


import com.xaosia.minebot.commands.discord.BotCmdHandler;
import com.xaosia.minebot.commands.minecraft.CommandHandler;
import com.xaosia.minebot.config.Settings;
import com.xaosia.minebot.events.BotReadyEvent;
import com.xaosia.minebot.listeners.CommandListener;
import com.xaosia.minebot.listeners.SessionListener;
import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.ChannelLogger;
import com.xaosia.minebot.utils.Chat;
import com.xaosia.minebot.utils.IDiscordClient;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import com.xaosia.minebot.listeners.ChatListener;

import javax.security.auth.login.LoginException;

public class MineBot extends JavaPlugin {

    private static MineBot instance;

    private static JDA client;
    private static IDiscordClient discord;
    private static BotCmdHandler botCmdHandler;
    private CommandHandler commandHandler;

    public void onEnable() {
        instance = this;

        //initialize settings
        Settings.get().setup();

        initJDA();

        //register minecraft listeners
        registerListener(new SessionListener());
        registerListener(new ChatListener());
        registerListener(new CommandListener());

        //register commands
        commandHandler = new CommandHandler(this);
        commandHandler.registerCommands();
    }

    public void onDisable() {
        shutdown();
    }

    public static MineBot get() {
        return instance;
    }

    public static JDA getClient() {
        return client;
    }

    public static IDiscordClient getDiscord() {
        return discord;
    }

    public void shutdown() {

        ChannelLogger.logStatus(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                .setDescription("The server is now offline")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_RED).build()));

        client.shutdown();
    }

    private void initJDA() {
        try {
            client = new JDABuilder(AccountType.BOT).setToken(Settings.getToken()).buildAsync();
            discord = new IDiscordClient();
            botCmdHandler = new BotCmdHandler();

            registerDiscordListener(botCmdHandler);
            registerDiscordListener(new BotReadyEvent());

            botCmdHandler.registerCommands();

        } catch (LoginException | RateLimitedException e) {
            e.printStackTrace();
        }
    }


    public void registerListener(Listener l) {
        Bukkit.getServer().getPluginManager().registerEvents(l, this);
    }

    public void registerDiscordListener(ListenerAdapter l) {
        client.addEventListener(l);
    }

}
