package com.xaosia.minebot.tasks;

import com.xaosia.minebot.utils.ChannelLogger;
import net.dv8tion.jda.core.MessageBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.Chat;
import com.xaosia.minebot.utils.DateUtils;

public class MinuteRestartTask extends BukkitRunnable {

        @Override
        public void run() {

            ChannelLogger.logStatus(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                    .setDescription("Restarting in " + DateUtils.secondsToString(RestartTask.getCountdown()))
                    .setFooter("System time | " + Bot.getBotTime(), null)
                    .setColor(Chat.CUSTOM_RED).build()));

            Bukkit.broadcastMessage(ChatColor.RED + "Server restarting in " + DateUtils.secondsToString(RestartTask.getCountdown()));

        }

}
