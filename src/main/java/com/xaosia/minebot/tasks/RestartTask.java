package com.xaosia.minebot.tasks;


import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.ChannelLogger;
import com.xaosia.minebot.utils.Chat;
import net.dv8tion.jda.core.MessageBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import com.xaosia.minebot.MineBot;
import com.xaosia.minebot.utils.DateUtils;

public class RestartTask extends BukkitRunnable {

    private static int countdown;
    private boolean minutes;
    private MinuteRestartTask minuteRestartTask;

    public RestartTask(int time, boolean minute) {
        this.minutes = minute;

        if (minute) {
            countdown = time * 60;
            minuteRestartTask = new MinuteRestartTask();
            minuteRestartTask.runTaskTimer(MineBot.get(), 60 * 20, 60 * 20);
        } else {
            countdown = time;
        }

    }

    @Override
    public void run() {
        countdown--;

        if (countdown == 0) {
            this.cancel();
            Bukkit.getServer().shutdown();
        }

        if (countdown < 11) {

            if (minutes) {
                minuteRestartTask.cancel();
            }

            ChannelLogger.logStatus(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                    .setDescription("Restarting in " + DateUtils.secondsToString(countdown))
                    .setFooter("System time | " + Bot.getBotTime(), null)
                    .setColor(Chat.CUSTOM_RED).build()));

            Bukkit.broadcastMessage(ChatColor.RED + "Server restarting in " + DateUtils.secondsToString(countdown));

            if (Bukkit.getVersion().contains("1.8") || Bukkit.getVersion().contains("1.7")) {

                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    player.playSound(player.getLocation(), Sound.valueOf("NOTE_BASS"), 1, 1);
                }

            }

            if (Bukkit.getVersion().contains("1.9") || Bukkit.getVersion().contains("1.10") || Bukkit.getVersion().contains("1.11") ||
                    Bukkit.getVersion().contains("1.12")) {

                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    player.playSound(player.getLocation(), Sound.valueOf("BLOCK_NOTE_BASS"), 1, 1);
                }

            }

        }

    }

    public static int getCountdown() {
        return countdown;
    }

}
