package com.xaosia.minebot.events;

import com.xaosia.minebot.utils.ChannelLogger;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import com.xaosia.minebot.utils.Bot;
import com.xaosia.minebot.utils.Chat;

public class BotReadyEvent extends ListenerAdapter {

    @Override
    public void onReady(ReadyEvent event) {

        ChannelLogger.logStatus(new MessageBuilder().setEmbed(Chat.getEmbed().setTitle("Server Status", null)
                .setDescription("The server is now online, you may now connect")
                .setFooter("System time | " + Bot.getBotTime(), null)
                .setColor(Chat.CUSTOM_GREEN).build()));

    }

}
