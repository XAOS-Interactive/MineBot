package com.xaosia.minebot.config;


import com.xaosia.minebot.utils.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import com.xaosia.minebot.MineBot;

import java.io.File;
import java.util.List;
import java.util.logging.Level;

public class Settings {


    private static Settings instance;

    public static Settings get() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    //config
    private static FileConfiguration config;
    private File configFile;

    //discord settings
    private static String token;
    private static String clientId;
    private static String ownerId;
    private static String prefix;
    private static String chatLogId;
    private static String commandLogId;
    private static String joinLogId;
    private static String statusLogId;
    private static List<String> cmdChannels;


    public void setup() {

        //copy default config file
        MineBot.get().getLogger().log(Level.INFO, "Loading configs");

        try {

            if (!MineBot.get().getDataFolder().exists()) {
                MineBot.get().getDataFolder().mkdir();
            }

            configFile = new File(MineBot.get().getDataFolder(), "config.yml");
            if (!configFile.exists()) {
                FileUtils.loadFile("config.yml");
            }

            reloadConfig();

            loadSettings();

        } catch (Exception ex) {
            MineBot.get().getLogger().log(Level.SEVERE, "Could not load configs", ex);
        }
    }

    public static FileConfiguration getConfig() {
        return config;
    }

    public void reloadConfig() {
        try {

            config = new YamlConfiguration();
            config.load(configFile);

        } catch (Exception ex) {
            MineBot.get().getLogger().log(Level.SEVERE, "Could not reload config: " + configFile.getName(), ex);
        }
    }

    public void saveConfig() {
        try {
            config.save(configFile);
        } catch (Exception ex) {
            MineBot.get().getLogger().log(Level.SEVERE, "Could not save config: " + configFile.getName(), ex);
        }
    }

    public void loadSettings() {
        token = getConfig().getString("token", null);
        clientId = getConfig().getString("clientId", null);
        ownerId = getConfig().getString("ownerId", null);
        prefix = getConfig().getString("prefix", "!");
        chatLogId = getConfig().getString("chatLogId", null);
        commandLogId = getConfig().getString("commandLogId", null);
        joinLogId = getConfig().getString("joinLogId", null);
        statusLogId = getConfig().getString("statusLogId", null);
        cmdChannels = getConfig().getStringList("cmdChannels");
    }

    public static String getToken() {
        return token;
    }

    public static String getClientId() {
        return clientId;
    }

    public static String getOwnerId() {
        return ownerId;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static String getChatLogId() {
        return chatLogId;
    }

    public static String getCommandLogId() {
        return commandLogId;
    }

    public static String getJoinLogId() {
        return joinLogId;
    }

    public static String getStatusLogId() {
        return statusLogId;
    }

    public static List<String> getCmdChannels() {
        return cmdChannels;
    }

}
