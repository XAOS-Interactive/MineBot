package com.xaosia.minebot.commands.minecraft.admin;


import com.xaosia.minebot.MineBot;
import com.xaosia.minebot.commands.minecraft.AbstractCommand;
import com.xaosia.minebot.exceptions.CommandException;
import com.xaosia.minebot.tasks.RestartTask;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class RestartCommand extends AbstractCommand {

    public RestartCommand() {
        super("restart", "<time> <true/false>");
    }

    @Override
    public boolean execute(final CommandSender sender, final String[] args) throws CommandException {
        if (args.length == 0) {
            return false;
        }

        int time = parseInt(args[0]);

        boolean minutes = parseBoolean(args[1]);
        new RestartTask(time, minutes).runTaskTimer(MineBot.get(), 20, 20);

        return true;
    }

    @Override
    public List<String> tabComplete(final CommandSender sender, final String[] args) {
        List<String> toReturn = new ArrayList<>();

        if (args.length == 2) {
            toReturn.add("true");
            toReturn.add("false");
        }

        return toReturn;
    }

}
