package com.xaosia.minebot.commands.discord.punish;

import com.xaosia.minebot.commands.discord.DiscordCmd;
import com.xaosia.minebot.exceptions.CommandException;
import com.xaosia.minebot.utils.Chat;
import com.xaosia.minebot.utils.PunishUtils;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

public class MuteCommand extends DiscordCmd {

    public MuteCommand() {
        super("mute", CommandType.PUNISH, "<user> <time> <reason>");
    }

    @Override
    public boolean onCommand(Guild guild, TextChannel channel, Member sender, Message message, String[] args) throws CommandException {

        if (args.length < 3) {
            return false;
        }

        String user = args[0];
        String time = args[1];
        String reason = args[2];

        PunishUtils.mute(user, time, reason);

        Chat.sendMessage(sender.getAsMention() + " The user " + user + " has been muted for " + reason, channel, 20);

        return true;
    }

}
