package com.xaosia.minebot.commands.discord.general;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import com.xaosia.minebot.commands.discord.DiscordCmd;
import com.xaosia.minebot.exceptions.CommandException;
import com.xaosia.minebot.utils.Chat;
import com.xaosia.minebot.utils.WhitelistUtils;

public class WhitelistCommand extends DiscordCmd {

    public WhitelistCommand() {
        super("whitelist", CommandType.GENERAL, "<add|remove> <user>");
    }

    @Override
    public boolean onCommand(Guild guild, TextChannel channel, Member sender, Message message, String[] args) throws CommandException {

        if (args.length < 2) {
            return false;
        }

        String user = args[1];

        if (args[0].equalsIgnoreCase("add")) {
            WhitelistUtils.whitelist(user);
            Chat.sendMessage(sender.getAsMention() + " The user " + user + " has been whitelisted and can now join the server!", channel, 20);
            return true;
        }

        if (args[0].equalsIgnoreCase("remove")) {
            WhitelistUtils.unwhitelist(user);
            Chat.sendMessage(sender.getAsMention() + " The user " + user + " has been unwhitelisted and can no longer join the server!", channel, 20);
            return true;
        }

        return false;
    }

}
