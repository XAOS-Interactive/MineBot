package com.xaosia.minebot.commands.discord.punish;

import com.xaosia.minebot.commands.discord.DiscordCmd;
import com.xaosia.minebot.exceptions.CommandException;
import com.xaosia.minebot.utils.Chat;
import com.xaosia.minebot.utils.PunishUtils;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

public class BanCommand extends DiscordCmd {

    public BanCommand() {
        super("ban", CommandType.PUNISH, "<user> [time] <reason>");
    }

    @Override
    public boolean onCommand(Guild guild, TextChannel channel, Member sender, Message message, String[] args) throws CommandException {

        if (args.length < 2) {
            return false;
        }

        if (args.length == 2) {
            String user = args[0];
            String reason = args[1];

            PunishUtils.ban(user, reason);
            Chat.sendMessage(sender.getAsMention() + " The user " + user + " has been banned for " + reason, channel, 20);
        }

        if (args.length == 3) {
            String user = args[0];
            String time = args[1];
            String reason = args[2];


            PunishUtils.ban(user, time, reason);
            Chat.sendMessage(sender.getAsMention() + " The user " + user + " has been banned for " + reason, channel, 20);
        }


        return true;
    }

}
